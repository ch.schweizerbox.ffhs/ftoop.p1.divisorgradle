

import java.util.concurrent.ExecutionException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import student.TestCase;

/**
 * Hinweis: Die Unit Tests haben einen festen Timeout von 10 sekunden - achten
 * Sie daher darauf, dass Sie das Testintervall nicht zu gross gestalten.
 * 
 * @author ble
 * 
 */

public class CalculateDivisorTest extends TestCase {

	@Test
	public void testCalculate() throws InterruptedException, ExecutionException {
		CalculateDivisorMain.main(new String[] { "10", "10000", "4", "0" });
		assertFuzzyEquals(
				"Zahl mit maximaler Anzahl Divisoren: 7560.0 (64.0 Divisoren)\n",
				systemOut().getHistory());
	}

	@Test
	public void testCalculate2() throws InterruptedException, ExecutionException {
		CalculateDivisorMain.main(new String[] { "100", "3285", "5", "0" });
		assertFuzzyEquals(
				"Zahl mit maximaler Anzahl Divisoren: 2520.0 (48.0 Divisoren)\n",
				systemOut().getHistory());
	}
	
	@Test
	public void testBerechneBereichsGroesse() throws InterruptedException, ExecutionException {
		CalculateDivisorMain.main(new String[] { "100", "3285", "5", "0" });
		CalculateDivisorMain cmt = new CalculateDivisorMain();
		assertEquals(637, cmt.berechneBereichsGroesse(100, 3285, 5));
		
	}
	
	
}
